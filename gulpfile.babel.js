import gulp from 'gulp';
import plumber from 'gulp-plumber';
import minifyCSS from 'gulp-minify-css';
import concat from 'gulp-concat';
import ejs from 'gulp-ejs';
import rename from 'gulp-rename';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import uglify from 'gulp-uglify';
import sourcemaps from 'gulp-sourcemaps';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import cssnext from 'postcss-cssnext';
import ftp from 'gulp-ftp';
import gutil from 'gulp-util';
import fs from 'fs';

gulp.task('shop:views', () => {
  const json = JSON.parse(fs.readFileSync('package.json'));
  gulp.src(`./src/shop/views/**/!(_)*.ejs`)
  .pipe(plumber())
  .pipe(ejs())
  .pipe(rename({
    extname: '.phtml'
  }))
  .pipe(gulp.dest('ZendApplication/application/views/scripts/shop'))
});

gulp.task('shop:layouts', () => {
  const json = JSON.parse(fs.readFileSync('package.json'));
  gulp.src(`./src/shop/layouts/**/!(_)*.ejs`)
  .pipe(plumber())
  .pipe(ejs())
  .pipe(rename({
    extname: '.phtml'
  }))
  .pipe(gulp.dest('ZendApplication/application/views/layouts'));
});

gulp.task('shop:js', () => {
  browserify({
    entries: ['src/shop/scripts/app.js'],
    debug: true
  })
  .bundle()
  .pipe(source('main.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({
    loadMaps: true
  }))
  // .pipe(uglify())
  .on('error', gutil.log)
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('ZendApplication/public_html/shop/scripts'))
});  

gulp.task('shop:scss', () => {
  return gulp
    .src('./src/shop/scss/**/!(_)*.scss')
    .pipe(sass())
    .pipe(postcss([cssnext()]))
    .pipe(gulp.dest('src/shop/css'));
});

gulp.task('shop:css', ['shop:scss'], () => {
  gulp
    .src('./src/shop/css/**/*.css')
    .pipe(concat('main.css'))
    .pipe(gulp.dest('ZendApplication/public_html/shop/stylesheets'))
})

gulp.task('shop:public', () => {
  gulp.src('src/shop/public/**/*', {
    base: 'src/shop/public'
  })
  .pipe(gulp.dest('ZendApplication/public_html/shop/'));
});

gulp.task('shop:watch', () => {
  gulp.watch('src/shop/public/**/*', ['shop:public']);
  gulp.watch('src/shop/views/**/*.ejs', ['shop:views']);
  gulp.watch('src/shop/layouts/**/*.ejs', ['shop:layouts']);
  gulp.watch('src/shop/scripts/**/*.js', ['shop:js']);
  gulp.watch('src/shop/scss/**/*.scss', ['shop:scss']);
});

gulp.task('ftp', () => {
  const opts = {
    host: 'ppark.xsrv.jp',
    user: 'ppark',
    pass: 'i42hgnih',
    remotePath: '/ppark.xsrv.jp/'
  }
  gulp.src('./ZendApplication/')
    .pipe(ftp(opts))
    .pipe(gutil.noop());
});

gulp.task('shop', ['shop:watch', 'shop:js', 'shop:public', 'shop:views', 'shop:layouts']);